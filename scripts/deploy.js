#! /usr/bin/env node

{
    const util = require("util");
    const exec = util.promisify(require("child_process").exec);

    const init = () => {
        exec("contentful extension update --force")
            .then(result => console.log("wow great", result))
            .catch(error => console.log("oh no", error));
    };

    init();
}
