import React from "react";
import ReactDOM from "react-dom";
import { init as initContentfulExtension } from "contentful-ui-extensions-sdk";
import { Extension } from "./components";
import "./index.css";

/**
 * initialise a contentful extension and parse the sdk
 */

initContentfulExtension(sdk => {
    ReactDOM.render(<Extension sdk={sdk} />, document.getElementById("root"));
});

if (module.hot) {
    module.hot.accept();
}
