// @flow

//import style from "./style.module.scss";
import "./style.css";
import React, { Component } from "react";
import { Button } from "@contentful/forma-36-react-components";
// import React, { Component, Fragment } from "react";
// import { Button, TextLink } from "@contentful/forma-36-react-components";

/**
 * Actions
 */
export default class Actions extends Component<*> {
    /**
     * Render
     */
    render() {
        const { userCurrent, userCreated, update } = this.props;
        console.log("user", userCurrent);
        if (userCurrent === userCreated) {
            return (
                <div>
                    <p>waiting for review from Tom</p>
                </div>
            );
        }

        if (userCurrent === "tom") {
            return (
                <div>
                    <p>
                        <b>Are changes Ok to publish?</b>
                    </p>
                    <Button
                        className="button-status"
                        buttonType="primary"
                        size="small"
                        onClick={() => update("Published")}
                    >
                        Yes
                    </Button>
                </div>
            );
        }

        return "Not Bert";
    }
}
