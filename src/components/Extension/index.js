// @flow

import React, { Component } from "react";
import { Actions, Status } from "../../components";
import { Button } from "@contentful/forma-36-react-components";
import "@contentful/forma-36-react-components/dist/styles.css";
import "@contentful/forma-36-fcss/dist/styles.css";
import "./style.css";

/**
 * Extension
 */
export default class Extension extends Component<*, *> {
    state = {
        status: this.props.sdk.entry.fields.reviewState._fieldLocales.en._value,
        user: this.props.sdk.entry.fields.user._fieldLocales.en._value,
        userRoles: null,
        loading: true,
        sdk: this.props.sdk,
    };
    /**
     * Component Mount => Setup Contentful UI extension
     */

    componentDidMount() {
        if (!this.state.sdk.user) {
            this.setState({
                loading: true,
            });
        } else {
            this.setState({
                loading: false,
            });
        }
    }

    componentDidUpdate() {
        this.props.sdk.window.startAutoResizer();
    }

    readyForPublish(status: *) {
        this.props.sdk.entry.fields.readyForPublish.setValue(status, "en");
    }

    updateStatus(status: *) {
        const { sdk } = this.props;
        const field = sdk.entry.fields.reviewState;
        field.setValue(status, "en");
        this.setState({ status: status });
        if (status === "Published") {
            this.readyForPublish("Yes");
        } else {
            this.readyForPublish("");
        }
    }

    /**
     * Render => Contentful UI Extension
     */

    render() {
        const { sdk } = this.props;
        const currentUser = this.state.user;
        const createdUser = "bert";
        //const currentUser = sdk.user.sys.id;
        //const createdUser = sdk.contentType.sys.createdBy.sys.id;
        if (this.state.loading) {
            return "loading";
        }
        if (!this.state.status) {
            return (
                <Button
                    className="button-status"
                    buttonType="primary"
                    size="small"
                    onClick={() => this.updateStatus("Ready for Review")}
                >
                    Ask Review
                </Button>
            );
        }
        if (this.state.status === "Published") {
            return <p>Page creation approved by Tom</p>;
        }
        return (
            <div>
                <Status status={this.state.status} />
                <Actions
                    userCurrent={currentUser}
                    userCreated={createdUser}
                    sdk={sdk}
                    update={status => this.updateStatus(status)}
                />
            </div>
        );
    }
}
