// @flow

//import style from "./style.module.scss";
import "./style.css";
import React, { useState } from "react";
import {
    Dropdown,
    DropdownList,
    Button,
    TextLink,
} from "@contentful/forma-36-react-components";
import { StatusList } from "../..";

/**
 * Status
 */
const StatusChange = ({ userRoles, status, statusList, statusUpdate }: *) => {
    const [open, setOpen] = useState(false);
    console.log("test: ", userRoles);

    const handleDropdown = status => {
        setOpen(false);
        statusUpdate(status);
    };

    // const changes = sdk.entry.fields.readyForReview._fieldLocales.en._value;
    // const legal = sdk.entry.fields.reviewedByLegal._fieldLocales.en._value;

    if (
        userRoles.includes("Editor") === true
        // || sdk.user.spaceMembership.admin === true
    ) {
        if (status === "Published") {
            return (
                <div>
                    <TextLink
                        linkType="primary"
                        onClick={() => statusUpdate("Needs changes")}
                    >
                        Needs Changes
                    </TextLink>
                </div>
            );
        }

        return (
            <div>
                <div>
                    <div className="dropdown-container">
                        <Dropdown
                            className="dropdown"
                            isOpen={open}
                            onClose={() => setOpen(false)}
                            position="bottom-left"
                            toggleElement={
                                <Button
                                    className="button-status"
                                    buttonType="primary"
                                    size="small"
                                    indicateDropdown
                                    onClick={() => setOpen(!open)}
                                >
                                    Change Status
                                </Button>
                            }
                        >
                            <DropdownList className="dropdownList">
                                <StatusList
                                    currentStatus={status}
                                    statusList={statusList}
                                    handleDropdown={status =>
                                        handleDropdown(status)
                                    }
                                />
                            </DropdownList>
                        </Dropdown>
                    </div>
                </div>
            </div>
        );
    }
    if (userRoles.includes("Author") === true) {
        return (
            <div>
                <p>no Needs</p>
            </div>
        );
    }
    if (userRoles.includes("Legal") === true) {
        if (status === "Needs Legal Review") {
            return (
                <div>
                    <div className="dropdown-container">
                        <Button
                            className="button-status"
                            buttonType="primary"
                            onClick={() => statusUpdate("Ready for Review")}
                        >
                            Ready for Review
                        </Button>
                        <TextLink
                            linkType="primary"
                            onClick={() => statusUpdate("Needs changes")}
                        >
                            Needs Changes
                        </TextLink>
                    </div>
                </div>
            );
        }
        return;
    }
};
export default StatusChange;
