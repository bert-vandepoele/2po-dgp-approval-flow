// @flow

// import style from "./style.module.scss";

import React, { Component, Fragment } from "react";
import { ProgressList } from "../..";

/**
 * Editor
 */
export default class Editor extends Component<*> {
    getProgressList() {
        const { userRoles, legal } = this.props;
        if (userRoles.includes("Editor") === true) {
            return <ProgressList legal={legal} />;
        }

        return "";
    }

    /**
     * Render
     */
    render() {
        return <Fragment>{this.getProgressList()}</Fragment>;
    }
}
