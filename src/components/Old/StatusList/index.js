// @flow

//import style from "./style.module.scss";
import React, { Component, Fragment } from "react";
import { StatusListItem } from "../..";

/**
 * StatusList
 */
export default class StatusList extends Component<*> {
    // Initial state

    /**
     * Render
     */
    render() {
        const { statusList, legal, handleDropdown, currentStatus } = this.props;

        const noLegalArray = [
            "Needs Legal Review",
            "Approved by Legal",
            currentStatus,
        ];
        // const statusArray = sdk.entry.fields.reviewState.validations[0].in;
        const statusArray = statusList.filter(function(item) {
            if (legal === false) {
                return !noLegalArray.includes(item);
            }
            return statusList;
        });

        const statusUpdate = statusArray.filter(obj => obj !== currentStatus);

        return (
            <Fragment>
                {statusUpdate.map((item, index) => (
                    <StatusListItem
                        key={index}
                        status={item}
                        handleDropdown={handleDropdown}
                    />
                ))}
            </Fragment>
        );
    }
}
