// @flow

//import style from "./style.module.scss";
import "./style.css";

import React, { Component } from "react";
import { Icon, Paragraph } from "@contentful/forma-36-react-components";

/**
 * ProgressList
 */
export default class ProgressList extends Component<*> {
    // Initial state

    /**
     * Render
     */
    render() {
        const { legal } = this.props;
        if (legal === true) {
            return (
                <div className="progressList">
                    <div>
                        <div
                            className={`icon ${
                                legal ? "progressList-done" : "default"
                            }`}
                        >
                            <Icon icon="CheckCircle" />
                            <Paragraph> Changes Done </Paragraph>
                        </div>
                    </div>
                    <div>
                        <div
                            className={`icon ${
                                legal ? "progressList-done" : "default"
                            }`}
                        >
                            <Icon icon="CheckCircle" />
                            <Paragraph>Waiting for Legal approval </Paragraph>
                        </div>
                    </div>
                    <div>
                        <div
                            className={`icon ${
                                legal ? "progressList-done" : "default"
                            }`}
                        >
                            <Icon icon="CheckCircle" />
                            <Paragraph> Legal Check Done </Paragraph>
                        </div>
                    </div>
                </div>
            );
        }
        return (
            <div className="progressList">
                <div>
                    <div className="icon progressList-done">
                        <Icon icon="CheckCircle" />
                        <Paragraph> Waiting for changes to be done </Paragraph>
                    </div>
                </div>
                <div>
                    <div
                        className={`icon ${
                            legal ? "progressList-done" : "default"
                        }`}
                    >
                        <Icon icon="CheckCircle" />
                        <Paragraph> Changes Done </Paragraph>
                    </div>
                </div>
            </div>
        );
    }
}
