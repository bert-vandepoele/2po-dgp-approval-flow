// @flow

//import style from "./style.module.scss";

import React, { Component } from "react";
import { DropdownListItem } from "@contentful/forma-36-react-components";

/**
 * StatusListItem
 */
export default class StatusListItem extends Component<*> {
    /**
     * Render
     */
    render() {
        const { status, handleDropdown } = this.props;

        return (
            <DropdownListItem onClick={() => handleDropdown(status)}>
                {status}
            </DropdownListItem>
        );
    }
}
