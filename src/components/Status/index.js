// @flow

//import style from "./style.module.scss";
import "./style.css";
import React, { Component, Fragment } from "react";
import { SectionHeading } from "@contentful/forma-36-react-components";

/**
 * Status
 */
export default class Status extends Component<*> {
    /**
     * Render
     */

    progress(status: *) {
        if (status === "Published") {
            return <div className="progress-bar-item published"> </div>;
        }

        if (status === "Needs Legal Review") {
            return <div className="progress-bar-item width-50"> </div>;
        }

        if (status === "Ready for Review") {
            return <div className="progress-bar-item width-75"> </div>;
        }

        return <div className="progress-bar-item"> </div>;
    }

    render() {
        const { status } = this.props;

        if (status === "Published") {
            return (
                <Fragment>
                    <div className="status publish">
                        <SectionHeading> {status} </SectionHeading>
                    </div>
                    <div className="progress-bar">{this.progress(status)}</div>
                </Fragment>
            );
        }

        return (
            <Fragment>
                <div className="status">
                    <SectionHeading> {status} </SectionHeading>
                </div>
                <div className="progress-bar">
                    <div className="progress-bar">{this.progress(status)}</div>
                </div>
            </Fragment>
        );
    }
}
