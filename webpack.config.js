const HtmlWebpackPlugin = require("html-webpack-plugin");
const HtmlWebpackInlineSourcePlugin = require("html-webpack-inline-source-plugin");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
    .BundleAnalyzerPlugin;
const StyleLintPlugin = require("stylelint-webpack-plugin");
const PostcssPresetEnv = require("postcss-preset-env");
const webpack = require("webpack");

module.exports = () => {
    return {
        output: {
            filename: "[name].[hash].js",
        },
        devServer: {
            overlay: true,
            hot: true,
        },
        module: {
            rules: [
                // Javascript
                {
                    enforce: "pre",
                    test: /\.(jsx?)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "eslint-loader",
                    },
                },
                {
                    test: /\.(jsx?)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader",
                    },
                },
                // Styles
                {
                    test: /\.(s?css|sass)$/,
                    use: [
                        "style-loader",
                        "css-loader",
                        "resolve-url-loader",
                        {
                            loader: "postcss-loader",
                            options: {
                                sourceMap: true,
                                plugins: [
                                    require("postcss-import"),
                                    require("postcss-will-change"),
                                    require("autoprefixer"),
                                    PostcssPresetEnv({ stage: 0 }),
                                ],
                            },
                        },
                        "sass-loader",
                    ],
                },
                // Images
                {
                    test: /\.(jpe?g|png|gif|svg)$/,
                    use: [
                        {
                            loader: "file-loader",
                            options: {
                                name: "[md5:hash:hex].[ext]",
                            },
                        },
                        {
                            loader: "image-webpack-loader",
                            options: {
                                optipng: {
                                    enabled: false,
                                },
                            },
                        },
                    ],
                },
            ],
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: "./src/index.html",
                filename: "./index.html",
                inlineSource: ".(jsx?|s?css|sass)$",
            }),
            new HtmlWebpackInlineSourcePlugin(),
            new BundleAnalyzerPlugin({
                analyzerMode: "static",
                reportFilename: __dirname + "/reports/report.html",
                openAnalyzer: false,
            }),
            new StyleLintPlugin({
                files: "**/**.css",
            }),
            new webpack.HotModuleReplacementPlugin(),
        ],
    };
};
